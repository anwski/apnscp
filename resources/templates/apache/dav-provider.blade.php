# No changes will be saved
# See hq.apnscp.com/service-overrides for help
<IfModule dav_module>
	<IfDefine !SLAVE>
		@foreach ($locations as $path => $provider)

		<Directory "{!! $prefix !!}{!! str_replace('"', '\\"', $path) !!}">
			Dav {{ $provider }}
		</Directory>
	@endforeach

	</IfDefine>
</IfModule>