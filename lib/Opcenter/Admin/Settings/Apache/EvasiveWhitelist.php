<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Apache;
	use Tivie\HtaccessParser\Parser;
	use Tivie\HtaccessParser\Token\Directive;

	class EvasiveWhitelist implements SettingsInterface
	{
		const CONFIG = '/etc/httpd/conf.d/evasive.conf';

		protected $parser;

		public function set($val): bool
		{
			if (\in_array($val, $this->get(), true)) {
				return true;
			}
			if (false === strpos($val, '*') && false === inet_pton($val)) {
				return error("Invalid IP address `%s'", $val);
			}
			$parser = $this->getParser()->parse();
			if (!($block = $parser->search('IfModule'))) {
				return error("Missing <IFModule directive in `%s'. This file is corrupt", self::CONFIG);
			}
			$block[0]->addChild(new Directive('DOSWhiteList', [$val]));
			file_put_contents(self::CONFIG, (string)$parser, LOCK_EX);
			Apache::reload();

			return true;
		}

		public function get()
		{
			if (!file_exists(self::CONFIG)) {
				return [];
			}
			$parser = $this->getParser();
			$ips = [];

			foreach ($parser->parse()->search('DOSWhiteList') as $directive) {
				$ips[] = $directive->getValue();
			}

			return $ips;
		}

		private function getParser(): Parser
		{
			if (null === $this->parser) {
				$this->parser = new Parser(new \SplFileObject(self::CONFIG));
				$this->parser->ignoreComments(false);
				$this->parser->ignoreWhitelines(false);
			}
			$this->parser->setContainer(new Apache\UnboundedHtaccessContainer());

			return $this->parser;
		}

		public function getHelp(): string
		{
			return 'Add whitelist to mod_evasive';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '127.0.0.1';
		}
	}