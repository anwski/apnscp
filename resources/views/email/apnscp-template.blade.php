@extends("common-layout")

@section('content')
    @component('mail::message')
    <table class="main" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="content-wrap">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-block">
                            @yield('body')
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    @endcomponent
@endsection

@section("footer")
    @include('common-footer')
@endsection