<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail;

	use Opcenter\Mail\Vacation\Contracts\VacationService;

	class Vacation
	{
		use \NamespaceUtilitiesTrait;

		public static function get(\Auth_Info_User $user = null): VacationService
		{
			$ns = self::appendNamespace('Vacation\\Providers\\' . self::getActiveService());
			/**
			 * @var Vacation\Providers\Mailbot $driver
			 */
			$driver = new $ns($user);

			return $driver;
		}

		public static function getActiveService(): string
		{
			return 'Mailbot';
		}
	}