<h3>{{ $target }}</h3>
<div id="control-file">
	<label class="b">
		<i class="fa fa-folder-o"></i>
		Docroot
	</label>
	{{ dirname($controlFile) }}
	<br/>
	<label class="b">
		<i class="fa fa-cogs"></i>
		App
	</label>
	{{  $Page->getPersonalityApp() }}
	<br/>
</div>

<h4>.htaccess Management</h4>

@if (false === $personalities)
	@include('master::partials.shared.htaccess-malformed', ['file' => $controlFile])
@else

	<form method="POST" id="add-form">
		<div id="add-directive">

			<legend>Add Directive</legend>
			<div class="form-inline form-group align-items-end">
				<div class="flex-column mr-3">
					<label for="personality" class="justify-content-start">
						Personality
					</label>
					<select id="personality" class="custom-select form-control" name="personality">
						@foreach ($Page->getPersonalities() as $p)
							<option value="{{ $p }}">{{ ucwords($p) }}</option>
						@endforeach
					</select>
				</div>
				<div class="flex-column mr-3">
					<label for="directive" class="justify-content-start">
						Directive
					</label>
					<select name="directive" id="directive" class="custom-select"></select>
				</div>
				<div class="flex-column mr-3">
					<label for="value">
						Value
					</label>
					<input id="directive-val" class="form-control" type="text" name="value" value=""
					       placeholder="Value"/>
				</div>
				<button class="main btn btn-primary" type="submit" id="add">
					<i class="fa fa-plus"></i>
					Add
				</button>
				<span id="ajax-image" class="ui-ajax-image ui-ajax-indicator"></span>
			</div>

			<div id="error-message" class="text-danger"></div>
			<div id="help"></div>
		</div>
	</form>

	@php
	$iterator = new RecursiveIteratorIterator(
		new RecursiveObjectIterator(
			$personalities
		),
		RecursiveIteratorIterator::SELF_FIRST
	);
	@endphp

	<form method="POST" id="htaccess-form">
		<button type="submit" name="save" value="Save Changes" class="main btn btn-primary">
			Save Changes
		</button>
		<ol class="list-unstyled mt-3" id="htaccess">
			@for ($i = 0, $iterator->next(), $level = 0; $iterator->valid(); $iterator->next(), $i++)
				@if (($p = $iterator->current()) instanceof \Tivie\HtaccessParser\Token\Block)
					{!! substr($Page->parseLine($p, $i), 0, -5) !!}
					<ol class="block list-unstyled">
						@foreach ($iterator->getChildren() as $p)
							{!! $Page->parseLine($p, null) !!}
						@endforeach
					</ol>
				@else
					{!! $Page->parseLine($p, $i) !!}
				@endif
			@endfor
		</ol>

		<input type="hidden" name="hash" value="{{ $Page->getHash() }}"/>
	</form>
@endif