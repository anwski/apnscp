<div class="alert alert-info">
	No databases found - create one through
	<a class="ui-action ui-action-switch-app ui-action-label"
	   href="/apps/change{{$Page->getMode()}}"
		>{{ \Template_Engine::init()->getActivePageName() }}
	</a>
</div>