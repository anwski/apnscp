@section("title", "Message from " . PANEL_BRAND)
@if (!\count(\array_values($report)))
	@component('email.indicator', ['status' => 'success'])
		All systems go
	@endcomponent
@else
	@component('email.indicator', ['status' => 'error'])
		Some sites are vulnerable!
	@endcomponent
@endif

@component('mail::message')
## Web App Overview
**Server name**: {{ SERVER_NAME }} ({{ \Opcenter\Net\Ip4::my_ip() }})<br/>
**Total failures**: {{ \count(\array_values($report)) }}

@component('mail::table')
| {{ str_repeat(' ', 20) }} | Type | Version | Latest |
|:----------------------------------------|:--------:|--------:|-------:|
@foreach ($report as $site => $apps)
| {{ sprintf('%-20s', "**${site}**") }} ||||
@foreach ($apps as $path => $info)
| {{ sprintf('%-20s', rtrim($info['hostname'] . '/' . $info['path'], '/')) }} | {{ ucwords($info['type']) }} | {{ $info['version'] }}  | {{ $info['latest'] }} |
@endforeach
@endforeach
@endcomponent

---

This is part of a monthly digest by {{ PANEL_BRAND }}. The following web apps are in a failed state
and will not be automatically upgraded as part of routine updates. This list may be generated at any
time from the terminal,

`cpcmd admin:list-failed-webapps`

Likewise to reset failures run the command,

`cpcmd admin:reset-webapp-failure`
@endcomponent
