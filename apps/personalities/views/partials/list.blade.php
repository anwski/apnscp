<h3>Hostnames</h3>
@php
	$kernel = Page_Container::kernelFromApp('webapps');
	$wapage = Page_Container::init($kernel);
	$proxy = \Module\Support\Webapps\App\UIPanel::instantiateContexted(\Auth::profile());
@endphp
<div class="row ui-webapp-panel">
@foreach (\Module\Support\Webapps::getAllHostnames(\Auth::profile()) as $hostname)
	@php
		$href = \HTML_Kit::page_url_params(
			['hostname' => $hostname]
		);
		$pane = $proxy->get($hostname, '');
	@endphp
	@if (!SCREENSHOTS_ENABLED)
		<div class="col-12 mb-1 hostname form-inline">
			@include('partials.actions')
			<div class="meta-inline form-control-static mx-2">
				<i title="{{ $pane->getApplicationType() }}"
				   class="app fa fa-{{ $pane->getApplicationType() }}"></i>
				<span class="version"></span>
			</div>
			<div class="form-control-static @if($pane->isSubdomain()) subdomain @else domain @endif">
				{{ $pane->getLocation() }}
			</div>
		</div>
	@else
		<div class="col-12 col-xl-3 col-lg-4 col-md-6">
			<div class="row">
				@include('master::partials.shared.wa-screenshot', [
					'pane'    => $pane,
					'Page'    => $wapage,
					'actions' => [
						'view' => 'partials.actions',
						'dropdownLocation' => 'dropdown-menu-right',
						'btnGroupClass' => 'btn-group-sm'
					]
				])

			</div>
		</div>
	@endif
@endforeach
</div>