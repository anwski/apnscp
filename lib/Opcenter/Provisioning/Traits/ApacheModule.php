<?php declare(strict_types=1);

	namespace Opcenter\Provisioning\Traits;

	use Event\Cardinal;
	use Event\Events;
	use Illuminate\Support\Str;
	use Opcenter\Provisioning\Apache;
	use Opcenter\SiteConfiguration;

	trait ApacheModule
	{
		/**
		 * Create new Apache configuration
		 *
		 * @param SiteConfiguration $container
		 * @param string            $cfgfile configuration file name
		 * @return bool
		 */
		public static function createModuleConfiguration(SiteConfiguration $container, string $cfgfile): bool
		{
			return Apache::writeConfiguration($container, static::getConfigurationName(), $cfgfile);
		}

		/**
		 * Get module configuration name
		 *
		 * @return string
		 */
		protected static function getConfigurationName(): string
		{
			return static::CONFIG_NAME;
		}

		/**
		 * Delete Apache module configuration
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function deleteModuleConfiguration(string $site): bool
		{
			$file = \Opcenter\Http\Apache::siteStoragePath($site) . '/' . static::getConfigurationName();

			if (!is_file($file)) {
				return true;
			}

			static::rebuildConfiguration();

			return unlink($file);
		}

		/**
		 * Keep configuration present, but remove from building
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function hideModuleConfiguration(SiteConfiguration $container): bool
		{
			$file = \Opcenter\Http\Apache::siteStoragePath($container->getSite()) . '/' . self::getConfigurationName();
			if (!file_exists($file)) {
				return true;
			}

			return rename($file, Str::replaceLast('/', '/.', $file));
		}

		/**
		 * Unhide a hidden Apache configuration
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function unhideModuleConfiguration(SiteConfiguration $container): bool
		{
			$file = \Opcenter\Http\Apache::siteStoragePath($container->getSite()) . '/.' . self::getConfigurationName();
			if (!file_exists($file)) {
				return true;
			}
			if (!rename($file, Str::replaceLast('/.', '/', $file))) {
				return false;
			}
			static::rebuildConfiguration();

			return true;
		}

		/**
		 * Rebuild httpd config
		 *
		 * @return bool
		 */
		public static function rebuildConfiguration()
		{
			Cardinal::register(
				[SiteConfiguration::HOOK_ID, Events::END],
				static function () {
					\Opcenter\Http\Apache::activate();
				}
			);

			return true;
		}
	}