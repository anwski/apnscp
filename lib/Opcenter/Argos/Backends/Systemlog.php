<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Systemlog extends Backend
	{
		protected $prio;
		protected $facility;
		protected $fmt;

		public function getAuthentication()
		{
			return null;
		}

		public function setAuthentication(...$vars): bool
		{
			return true;
		}

		public function setPrio(string $prio): bool
		{
			$known = [
				'EMERG',
				'ALERT',
				'CRIT',
				'ERR',
				'WARNING',
				'NOTICE',
				'INFO',
				'DEBUG'
			];
			$prio = strtoupper($prio);
			if (!\in_array($prio, $known, true)) {
				return error("Unknown priority `%s'", $prio);
			}
			$this->prio = $prio;

			return true;
		}

		public function setFacility(string $facility): bool
		{
			$known = [
				'KERN',
				'USER',
				'MAIL',
				'DAEMON',
				'AUTH',
				'LPR',
				'NEWS',
				'UUCP',
				'CRON',
				'SYSLOG',
				'LOCAL0',
				'LOCAL1',
				'LOCAL2',
				'LOCAL3',
				'LOCAL4',
				'LOCAL5',
				'LOCAL6',
				'LOCAL7',
			];
			$facility = strtoupper($facility);
			if (!\in_array($facility, $known, true)) {
				return error("Unknown facility `%s'", $facility);
			}
			$this->facility = $facility;

			return true;
		}
	}