<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Image_Color
	{
		/**
		 * Convert rgb triplet to hsl
		 *
		 * @param int $r red
		 * @param int $g green
		 * @param int $b blue
		 * @return array
		 */
		public static function rgb2hsl($r, $g, $b, $depth = 24)
		{
			$cs = pow(2, $depth / 3) - 1;
			$r /= $cs;
			$b /= $cs;
			$g /= $cs;
			$max = max($r, $g, $b);
			$min = min($r, $g, $b);

			// compute hue
			if ($max == $min) {
				$h = 0;
			} // achromatic
			else {
				if ($max == $r) {
					$h = (60 * ($g - $b) / ($max - $min) + 360) % 360;
				} else {
					if ($max == $g) {
						$h = 60 * ($b - $r) / ($max - $min) + 120;
					} else {
						$h = 60 * ($r - $g) / ($max - $min) + 240;
					}
				}
			}

			// compute lightness
			$l = .5 * ($max + $min);

			// compute saturation using l
			if ($max == $min) {
				$s = 0;
			} else {
				if ($l <= .5) {
					$s = ($max - $min) / 2 * $l;
				} else {
					$s = ($max - $min) / (2 - 2 * $l);
				}
			}

			return array($h, $s, $v);
		}

		/**
		 * Convert rgb triplet into hsv
		 *
		 * @param  int $r red
		 * @param  int $g green
		 * @param  int $b blue
		 * @return array
		 */
		public static function rgb2hsv($r, $g, $b, $depth = 24)
		{
			$cs = pow(2, $depth / 3) - 1;
			$r /= $cs;
			$b /= $cs;
			$g /= $cs;
			$max = max($r, $g, $b);
			$min = min($r, $g, $b);

			// compute hue
			if ($max == $min) {
				$h = 0;
			} // achromatic
			else {
				if ($max == $r) {
					$h = (60 * ($g - $b) / ($max - $min) + 360) % 360;
				} else {
					if ($max == $g) {
						$h = 60 * ($b - $r) / ($max - $min) + 120;
					} else {
						$h = 60 * ($r - $g) / ($max - $min) + 240;
					}
				}
			}

			if ($max == 0) {
				$s = 0;
			} else {
				$s = 1 - $min / $max;
			}

			$v = $max;

			return array($h, $s, $v);
		}

		/**
		 * Convert hsv triplet into rgb
		 *
		 * @param  int $h hue
		 * @param  int $s saturation
		 * @param  int $v value
		 * @return array
		 */
		public static function hsv2rgb($h, $s, $v, $depth = 24)
		{
			$cs = pow(2, $depth / 3) - 1;
			$hi = floor($h / 60) % 6;
			$f = $h / 60 - floor($h / 60);
			$p = $v * (1 - $s);
			$q = $v * (1 - $f * $s);
			$t = $v * (1 - (1 - $f) * $s);

			if ($hi == 0) {
				$cv = array($v, $t, $p);
			} else {
				if ($hi == 1) {
					$cv = array($q, $v, $p);
				} else {
					if ($hi == 2) {
						$cv = array($p, $v, $t);
					} else {
						if ($hi == 3) {
							$cv = array($p, $q, $v);
						} else {
							if ($hi == 4) {
								$cv = array($t, $p, $v);
							} else {
								if ($hi == 5) {
									$cv = array($v, $p, $q);
								}
							}
						}
					}
				}
			}

			return array_map(
				static function ($v) use ($cs) {
					return round($v * $cs);
				},
				$cv
			);
		}

		public static function randrgb(
			array $r = array(0, 255),
			$g = array(0, 255),
			array $b = array(0, 255)
		) {
			$r1 = mt_rand($r[0], $r[1]);
			$g1 = mt_rand($g[0], $g[1]);
			$b1 = mt_rand($b[0], $b[1]);

			return array($r1, $g1, $b1);
		}

		public static function randhsl(
			array $hue = array(0, 360),
			array $saturation = array(25, 75),
			array $lightness = array(45, 80)
		) {
			$h = mt_rand($hue[0], $hue[1]);
			$s = mt_rand($saturation[0], $saturation[1]) / 100;
			$l = mt_rand($lightness[0], $lightness[1]) / 100;

			return array($h, $s, $l);
		}

		public static function hsl2hex($h, $s, $l)
		{
			list($r, $g, $b) = self::hsl2rgb($h, $s, $l);

			return self::rgb2hex($r, $g, $b);
		}

		/**
		 * Convert hsl triplet into rgb
		 *
		 * @param  int $h hue
		 * @param  int $s saturation
		 * @param  int $l lightness
		 * @return array
		 */
		public static function hsl2rgb($h, $s, $l, $depth = 24)
		{
			$cs = pow(2, $depth / 3) - 1;
			// achromatic
			if ($s == 0) {
				return array_fill(0, 3, round($l * $cs));
			}

			if ($l < .5) {
				$q = $l * (1 + $s);
			} else {
				$q = $l + $s - ($l * $s);
			}

			$p = 2 * $l - $q;

			// normalize h to the range [0,1)
			$hk = $h / 360;

			$r = $hk + 1 / 3;
			if ($r < 0) {
				$r += 1;
			} else {
				if ($r > 1) {
					$r -= 1;
				}
			}

			$g = $hk;
			if ($g < 0) {
				$g += 1;
			} else {
				if ($g > 1) {
					$g -= 1;
				}
			}

			$b = $hk - 1 / 3;
			if ($b < 0) {
				$b += 1;
			} else {
				if ($b > 1) {
					$b -= 1;
				}
			}

			foreach (array('r', 'g', 'b') as $cn) {
				$c = $$cn;
				if ($c < 1 / 6) {
					$c = ($q - $p) * 6 * $c + $p;
				} else {
					if ($c >= 1 / 6 && $c < .5) {
						$c = $q;
					} else {
						if ($c >= .5 && $c < 2 / 3) {
							$c = ($q - $p) * 6 * (2 / 3 - $c) + $p;
						} else {
							$c = $p;
						}
					}
				}
				$$cn = $c;
			}

			return array(
				round($r * $cs),
				round($g * $cs),
				round($b * $cs)
			);
		}

		public static function rgb2hex($r, $g, $b)
		{
			return sprintf('#%X%X%X', $r, $g, $b);
		}

	}

?>
