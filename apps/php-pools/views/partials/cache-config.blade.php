<tr>
	<th class="pt-4">Cache configuration</th>
	<td>
		@php
			/** @var \Opcenter\Http\Php\Fpm\PoolStatus $pool */
			//$metrics = $Page->getConfig();
		@endphp
		<table class="table text-center">
			<thead>
				<th>
					Cache size
				</th>
			</thead>
			<tbody>
				<td>
					<select name="cache-size" class="custom-select">
						@foreach ([16, 32, 64, 128, 256, 512] as $size)
							<option value="{{ $size }}"
					            @if ($size === $config->getSize()) SELECTED="SELECTED" @endif>
								{{ $size }} @if ($size !== 'auto') MB @endif
							</option>
						@endforeach
					</select>
				</td>
			</tbody>
		</table>
	</td>
</tr>