<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\ServiceValidator;

	class Proclimit extends ServiceValidator
	{
		const DESCRIPTION = 'Limit account to # processes';
		const VALUE_RANGE = '[null, 0-4096]';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			}
			if (!\is_int($value) || $value < 1) {
				return error('Max CPU processes must be an positive integer >= 1');
			}
			if ($value < 100) {
				warn('proclimit values below 100 are known to cause problems with thread-intensive tasks and nvm');
			}

			return true;
		}
	}
