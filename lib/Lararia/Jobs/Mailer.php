<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */


namespace Lararia\Jobs;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Lararia\Jobs\Job\Intern;
use Lararia\Jobs\Job\Report;
use Lararia\Jobs\Traits\RawManipulation;

class Mailer {

	/**
	 * @var Job[]
	 */
	protected $jobs;
	/**
	 * @var Report
	 */
	protected $report;

	public function __construct($jobs)
	{

		if (!$jobs instanceof Collection) {
			$jobs = new Collection([$jobs]);
		}

		$this->jobs = $jobs;
		$this->report = new Report();

		$this->batch($jobs);
	}

	protected function batch(Collection $jobs): void
	{
		$jobs->each(function ($job) {
			$class = new class($job) {
				use RawManipulation;

				public function __construct($job)
				{
					$this->job = $job;
				}

				public function getJobId(): ?int
				{
					return (int)$this->job->id;
				}

				public function getConnectionName()
				{
					return $this->job->connection;
				}
			};
			// single job
			$errlog = null;
			if ($job instanceof \stdClass) {
				$job = \Util_PHP::unserialize(data_get(json_decode((string)$job->payload), 'data.command', null), true);
				$errlog = (array)json_decode($class->getField('erlog'), true);
				// gets lost when pulling all group jobs
				$jobId = $class->getJobId();
				$job->setId($jobId);
				$job->connection = $class->getConnectionName();
			}
			if (!$job instanceof Job) {
				fatal("Argument must be instance of %s", Job::class);
			}

			$this->report->add(new Intern($job, $errlog));
		});
	}

	/**
	 * Send notification
	 *
	 * @return bool mail sent
	 */
	public function send(): bool
	{
		$template = $this->report->getTemplate();
		// reverse ordering
		$head = $this->report->current();
		assert($head instanceof Job, 'report contains job objects');
		$email = $head->getEmail();
		$adminEmail = $head->getAdminCopyEmail();
		if (!$template) {
			return false;
		}

		if (!$email && !$adminEmail) {
			return false;
		}

		$c = new $template($this->report);
		$mail = Mail::to($email ?: $adminEmail);
		if ($email && $adminEmail && $email !== $adminEmail) {
			$mail->bcc($adminEmail);
		}
		$mail->send($c);

		return true;
	}
}