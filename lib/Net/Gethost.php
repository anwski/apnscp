<?php

	/**
	 * Net_Gethost utilities
	 * Based off work by Drew Phillips, TorUtils
	 *
	 * Project:  TorUtils: PHP classes for interacting with Tor
	 * File:     TorDNSEl.php
	 *
	 * Copyright (c) 2015, Drew Phillips
	 * All rights reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 *  - Redistributions of source code must retain the above copyright notice,
	 *    this list of conditions and the following disclaimer.
	 *  - Redistributions in binary form must reproduce the above copyright notice,
	 *    this list of conditions and the following disclaimer in the documentation
	 *    and/or other materials provided with the distribution.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
	 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	 * POSSIBILITY OF SUCH DAMAGE.
	 *
	 * Any modifications to the library should be indicated clearly in the source code
	 * to inform users that the changes are not a part of the original software.
	 *
	 * @copyright 2015 Drew Phillips
	 * @author    Drew Phillips <drew@drew-phillips.com>
	 *
	 */
	class Net_Gethost
	{
		const DNS_PORT = 53;

		protected $timeout;
		protected $nameserver;
		protected $port;

		public function __construct(
			$timeout = 1500,
			$dnsServer = Dns_Module::RECURSIVE_NAMESERVERS,
			$dnsPort = self::DNS_PORT
		) {
			$this->timeout = $timeout;
			$dns = (array)$dnsServer;
			$this->nameserver = \Opcenter\Net\IpCommon::format($dns[array_rand($dns)]);
			$this->port = $dnsPort;
		}

		public static function gethostbyaddr_t($addr, int $timeout = 1000, array $ns = \Dns_Module::RECURSIVE_NAMESERVERS)
		{
			// random transaction number (for routers etc to get the reply back)
			$data = rand(0, 99);
			// trim it to 2 bytes
			$data = substr($data, 0, 2);
			// request header
			$data .= "\1\0\0\1\0\0\0\0\0\0";
			// split IP up
			if (\Opcenter\Net\Ip6::valid($addr)) {
				$ip6 = '';
				$quads = explode(':', $addr);
				$fill = 8-\count($quads);
				foreach ($quads as $quad) {
					$tmp = str_repeat('0.', 4-strlen($quad));
					if ($quad === '') {
						// only happens once, RFC 3513, section 2.2
						$tmp = str_repeat($tmp, $fill+1);
					}
					$ip6 .= '.' . rtrim($tmp . implode('.', preg_split('//', $quad, 4, PREG_SPLIT_NO_EMPTY)), '.');
				}
				$addr = ltrim($ip6, '.');
			} else if (!\Opcenter\Net\Ip4::valid($addr)) {
				return error("invalid ip `%s' specified", $addr);
			}
			$bits = explode('.', $addr);

			// error checking
			if ($timeout > 15000 || $timeout <= 0) {
				return error('timeout value exceeds 15 seconds');
			}
			// there is probably a better way to do this bit...
			// loop through each segment
			$bitLength = \count($bits);
			for ($x = $bitLength - 1; $x >= 0; $x--) {
				// needs a byte to indicate the length of each segment of the request
				switch (strlen($bits[$x])) {
					case 1: // 1 byte long segment
						$data .= "\1";
						break;
					case 2: // 2 byte long segment
						$data .= "\2";
						break;
					case 3: // 3 byte long segment
						$data .= "\3";
						break;
					default: // segment is too big, invalid IP
						return error('segment length too long, invalid ip');
				}
				// and the segment itself
				$data .= $bits[$x];
			}
			// and the final bit of the request
			$data .= $bitLength === 4 ? "\7in-addr\4arpa\0\0\x0C\0\1" : "\3ip6\4arpa\0\0\x0C\0\1";
			$dns = $ns[array_rand($ns)];
			// create UDP socket
			$handle = fsockopen('udp://' . \Opcenter\Net\IpCommon::format($dns), self::DNS_PORT);
			socket_set_timeout($handle, (int)($timeout / 1000), $timeout % 1000 * 1000);
			// send our request (and store request size so we can cheat later)
			$requestsize = fwrite($handle, $data);
			// hope we get a reply
			$response = fread($handle, 1024);
			fclose($handle);
			// request timeout or incomplete read - original query is always returned
			if ($response === false || strlen($response) <= $requestsize) {
				return null;
			}
			$type = unpack('S', substr($response, $requestsize + 2));
			if ($type[1] != 0x0C00)  // answer
			{
				return null;
			}
			// set up our variables
			$host = '';
			// set our pointer at the beginning of the hostname
			// uses the request size from earlier rather than work it out
			$position = $requestsize + 12;
			// reconstruct hostname
			do {
				// get segment size
				$len = unpack('c', substr($response, $position));
				// null terminated string, so length 0 = finished
				if ($len[1] == 0) {
					// return the hostname, without the trailing .
					return substr($host, 0, -1);
				}
				// add segment to our host
				$host .= substr($response, $position + 1, $len[1]) . '.';
				// move pointer on to the next segment
				$position += $len[1] + 1;
			} while ($len != 0);

			// error - return the hostname we constructed (without the . on the end)
			return $addr;
		}

		public static function gethostbyname_t($name, $timeout = 1000, array $ns = \Dns_Module::RECURSIVE_NAMESERVERS)
		{
			if ((int)$timeout != $timeout) {
				return error("timeout value `%s' invalid", $timeout);
			}
			if ($timeout > 15000) {
				return error('timeout value exceeds 15 seconds');
			}
			$class = new self($timeout, $ns);

			return $class->lookup($name, DNS_ANY);

		}

		/**
		 * Perform a DNS lookup to the Tor DNS exit list service and determine
		 * if the remote connection could be a Tor exit node.
		 *
		 * @param string $host hostname in the designated tordnsel format
		 * @param int    $rr RR type
		 * @return null|string
		 */
		public function lookup($host, $rr = DNS_A): ?string
		{
			// if fully-qualified strip right-most marker
			$host = rtrim($host, '.');
			if ($rr === DNS_ANY) {
				foreach ([DNS_A, DNS_AAAA] as $rec) {
					if (null !== ($response = $this->lookup($host, $rec))) {
						return $response;
					}
				}

				return null;
			}

			$query = $this->generateQuery($host, $rr);
			$data = $this->performRawLookup($query);
			if (!$data) {
				return error('DNS request timed out');
			}
			$response = $this->parseResponse($data);
			$code = array_get($response, 'header.RCODE', 1);
			if (null !== ($reason = $this->parseRcode($code))) {
				return error("failed to query `%s': %s", $host, $this->parseRcode($code));
			}
			$last = array_pop($response['answers']);

			return $last['data'] ?? null;

		}

		/**
		 * Generate a DNS query to send to the DNS server.  This generates a
		 * simple DNS "A" query for the given hostname.
		 *
		 * @param string $host Hostname used in the query
		 * @param int    $type Record type
		 * @return string
		 */
		protected function generateQuery(string $host, int $type = DNS_A)
		{
			$id = rand(1, 0x7fff);
			$req = pack('n6',
				$id,   // Request ID
				0x100, // standard query
				1,     // # of questions
				0,     // answer RRs
				0,     // authority RRs
				0      // additional RRs
			);
			foreach (explode('.', $host) as $bit) {
				// split name levels into bits
				$l = strlen($bit);
				// append query with length of segment, and the domain bit
				$req .= chr($l) . $bit;
			}
			// null pad the name to indicate end of record
			$req .= "\0";
			$req .= pack('n2',
				log($type)/log(2)+1,
				1  // class IN
			);
			return $req;
		}

		/**
		 * Send UDP packet containing DNS request to the DNS server
		 *
		 * @param string $query DNS query
		 * @return string DNS response or empty string if request timed out
		 */
		protected function performRawLookup($query)
		{
			$fp = fsockopen('udp://' . $this->nameserver, $this->port, $errno, $errstr);
			if (!$fp) {
				return error("Failed to send DNS request. Error `%s': `%s'",
					$errno, $errstr
				);
			}
			fwrite($fp, $query);
			stream_set_timeout($fp, (int)($this->timeout / 1000), $this->timeout % 1000 * 1000);

			return fread($fp, 512);
		}

		/**
		 * Parses the DNS response
		 *
		 * @param string $data DNS response
		 * @return array|bool Array with parsed response
		 */
		protected function parseResponse($data)
		{
			$p = 0;
			$offset = array();
			$header = array();
			$rsize = strlen($data);
			if ($rsize < 12) {
				return error('malformed DNS response');
			}
			// read back transaction ID
			$id = unpack('n', substr($data, $p, 2));
			$p += 2;
			$header['ID'] = $id[1];
			// read query flags
			$flags = unpack('n', substr($data, $p, 2));
			$flags = $flags[1];
			$p += 2;
			// read flag bits
			$header['QR'] = ($flags >> 15);
			$header['Opcode'] = ($flags >> 11) & 0x0f;
			$header['AA'] = ($flags >> 10) & 1;
			$header['TC'] = ($flags >> 9) & 1;
			$header['RD'] = ($flags >> 8) & 1;
			$header['RA'] = ($flags >> 7) & 1;
			$header['RCODE'] = ($flags & 0x0f);
			// read count fields
			$counts = unpack('n4', substr($data, $p, 8));
			$p += 8;
			$header['QDCOUNT'] = $counts[1];
			$header['ANCOUNT'] = $counts[2];
			$header['NSCOUNT'] = $counts[3];
			$header['ARCOUNT'] = $counts[4];
			$records = array();
			$records['questions'] = array();
			$records['answers'] = array();
			$records['authority'] = array();
			$records['additional'] = array();
			for ($i = 0; $i < $header['QDCOUNT']; ++$i) {
				$records['questions'][] = $this->readQuestion($data, $p);
			}
			for ($i = 0; $i < $header['ANCOUNT']; ++$i) {
				$records['answers'][] = $this->readResourceRecord($data, $p);
			}
			for ($i = 0; $i < $header['NSCOUNT']; ++$i) {
				$records['authority'][] = $this->readResourceRecord($data, $p);
			}
			for ($i = 0; $i < $header['ARCOUNT']; ++$i) {
				$records['additional'][] = $this->readResourceRecord($data, $p);
			}

			return array(
				'header'     => $header,
				'questions'  => $records['questions'],
				'answers'    => $records['answers'],
				'authority'  => $records['authority'],
				'additional' => $records['additional'],
			);
		}

		/**
		 * Read a DNS question section
		 *
		 * @param string $data   The DNS response packet
		 * @param number $offset Starting offset of $data to begin reading
		 * @return array Array with question information
		 */
		protected function readQuestion($data, &$offset)
		{
			$question = array();
			$name = $this->readHostname($data, $offset);
			$type = unpack('n', substr($data, $offset, 2));
			$offset += 2;
			$class = unpack('n', substr($data, $offset, 2));
			$offset += 2;
			$question['name'] = $name;
			$question['type'] = $type[1];
			$question['class'] = $class[1];

			return $question;
		}

		/**
		 * Read a DNS name from a response
		 *
		 * @param string $data   The DNS response packet
		 * @param number $offset Starting offset of $data to begin reading
		 * @return string  The DNS name in the packet
		 */
		protected function readHostname($data, &$offset)
		{
			$name = array();
			do {
				/**
				 * @xxx BUG: large additional section records aren't fully read w/ verbose responses
				 *           check if record contains suggested hostname
				 */
				if (!isset($data[$offset])) {
					break;
				}
				$len = $data[$offset];
				++$offset;

				if ($len === "\0" || $len === '') {
					/* null terminator or */
					/* unpack(): Type C: not enough input, need 1 have 0 */
					break;
				} else if ($len === "\xC0") {
					// pointer or sequence of names ending in pointer
					$off = unpack('n', substr($data, $offset - 1, 2));
					++$offset;
					$noff = $off[1] & 0x3fff;
					$name[] = $this->readHostname($data, $noff);
					break;
				} else {
					// name segment precended by the length of the segment
					$len = unpack('C', $len);
					$name[] = substr($data, $offset, $len[1]);
					$offset += $len[1];
				}
			} while (true);
			return implode('.', $name);
		}

		/**
		 * Read a DNS resource record
		 *
		 * @param string $data   The DNS response packet
		 * @param int    $offset Starting offset of $data to begin reading
		 * @return array Array with RR information
		 */
		protected function readResourceRecord($data, &$offset)
		{
			$rr = array();
			$rr['name'] = $this->readHostname($data, $offset);

			$fields = @unpack('nTYPE/nCLASS/NTTL/nRDLENGTH', substr($data, $offset, 10));
			if (!$fields) {
				return;
			}
			$offset += 10;
			$rdata = substr($data, $offset, $fields['RDLENGTH']);
			$offset += $fields['RDLENGTH'];
			$rr['TYPE'] = $fields['TYPE'];
			$rr['CLASS'] = $fields['CLASS'];
			$rr['TTL'] = $fields['TTL'];
			$rr['SIZE'] = $fields['RDLENGTH'];
			$rr['RDATA'] = $rdata;
			switch ($rr['TYPE']) {
				/*
				A               1 a host address
				NS              2 an authoritative name server
				MD              3 a mail destination (Obsolete - use MX)
				MF              4 a mail forwarder (Obsolete - use MX)
				CNAME           5 the canonical name for an alias
				SOA             6 marks the start of a zone of authority
				MB              7 a mailbox domain name (EXPERIMENTAL)
				MG              8 a mail group member (EXPERIMENTAL)
				MR              9 a mail rename domain name (EXPERIMENTAL)
				NULL            10 a null RR (EXPERIMENTAL)
				WKS             11 a well known service description
				PTR             12 a domain name pointer
				HINFO           13 host information
				MINFO           14 mailbox or mail list information
				MX              15 mail exchange
				TXT             16 text strings
				*/
				case DNS_A: // A
					$addr = unpack('Naddr', $rr['RDATA']);
					$rr['data'] = long2ip($addr['addr']);
					break;
				case DNS_NS: // NS
				case 16: // TXT
					$temp = 0;
					$rr['data'] = $this->readHostname($rr['RDATA'], $temp);
					break;
				case 28: // AAAA
					$addr = unpack('A16addr', $rr['RDATA']);
					$rr['data'] = inet_ntop($addr['addr']);
				case 6: // SOA
					// Record unlikely to exist
					return $rr;
			}

			return $rr;
		}

		protected function parseRcode($code)
		{
			switch ($code) {
				case 0:
				case 3:
					// nxdomain
					return null;
				case 1:
					return 'The name server was unable to interpret the query.';
				case 2:
					return 'Server failure - The nameserver was unable to process this query due to misconfiguration.';
				case 4:
					return 'Not Implemented - The nameserver does not support the requested kind of query.';
				case 5:
					return 'Refused - The nameserver rejected this query. Please try again later.';
				default:
					return "Bad RCODE in DNS response.  RCODE = '{$code}'";
			}
		}
	}