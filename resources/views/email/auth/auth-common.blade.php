@component('mail::message')
    @unless(empty($config['domain']))
        <h3>Domain: {{ $domain }}</h3>
    @endunless
    @yield('notice')
    @unless(empty($config['geoip']))
        <br />
        @include("email.auth.partials.user-meta")
    @endunless
@endcomponent
