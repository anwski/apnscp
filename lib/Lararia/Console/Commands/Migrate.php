<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Database\Console\Migrations\MigrateCommand;
	use Illuminate\Support\Facades\Artisan;
	use Lararia\Database\Migrations\DatabaseMigrationRepository;

	class Migrate extends MigrateCommand
	{
		/**
		 * The console command description.
		 *
		 * @var string
		 */

		protected $description = 'Run the database + platform migrations';


		public function __construct()
		{
			$migrator = app('migrator');
			$this->signature .= '{--dbonly : Run a database migration only.}
                {--platform : Run a platform migration instead.}';
			parent::__construct($migrator);
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if (!$this->confirmToProceed()) {
				return;
			}
			$this->input->setOption('force', true);

			if ($this->option('dbonly')) {
				return parent::handle();
			}

			// migrator is singleton, flip source back to db
			$this->migrator->getRepository()->setType('db');
			parent::handle();

			if ($this->option('platform')) {
				return Artisan::call('migrate:platform', ['--force' => $this->option('force')]);
			}

			Artisan::call('migrate:platform', ['--force' => $this->option('force')], $this->output);

		}

		public function getHelp()
		{
			return 'Default runs database, then platform migration.' . "\n" .
				'--dbonly : Run a database migration only.' . "\n" .
				'--platform : Run a platform migration instead.';
		}


	}
