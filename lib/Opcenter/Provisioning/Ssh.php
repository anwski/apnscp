<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Filesystem\Whiteout;
	use Opcenter\Map;
	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Ssh
	 *
	 * @package Opcenter\Provisioning
	 */
	class Ssh
	{
		// file excluded when [ssh] => crontab_link disabled
		const PAM_WHITEOUT_FILE = '/etc/pam.d/sshd';

		use FilesystemPopulatorTrait;
		use GroupCreationTrait;

		const TEMPLATE_FILES = [
			'/etc/' . \Ssh_Module::PAM_SVC_NAME . '.pamlist' => ['root', 'root', 0644],
			'/etc/gemrc'                                     => [null, null, 0644],
			'/etc/Muttrc.local'                              => [null, null, 0644]
		];

		const SUPPLEMENTAL_GROUPS = [
			'screen' => ['screen', null],
		];

		/**
		 * Add a domain to domain database
		 *
		 * @param int    $port
		 * @param string $site
		 * @return bool
		 */
		public static function mapPort(int $port, string $site): bool
		{
			return static::mapWrap((string)$port, $site);
		}

		/**
		 * Add or remove a domain
		 *
		 * @param null|string $site   site or null to remove
		 * @param string      $domain domain name
		 * @return bool
		 */
		private static function mapWrap(?string $site, string $domain): bool
		{
			$ret = true;
			foreach ([Map::DOMAIN_TXT_MAP, Map::DOMAIN_MAP] as $db) {
				$map = Map::load($db, 'cd');
				if (null === $site) {
					$ret &= !$map->exists($domain) || $map->delete($domain);
				} else {
					$ret &= $map->insert($domain, $site);
				}
			}

			return (bool)$ret;
		}

		/**
		 * Remove domain from db
		 *
		 * @param int $port
		 * @return bool
		 */
		public static function unmapDomain(int $port): bool
		{
			return static::mapWrap(null, (string)$port);
		}

		/**
		 * Deny SSH access while enabling other requisite services
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public static function blockSsh(SiteConfiguration $svc): bool
		{
			$whiteout = new Whiteout($svc->getAccountRoot());
			return $whiteout->blocked(self::PAM_WHITEOUT_FILE) || $whiteout->whiteout(self::PAM_WHITEOUT_FILE);
		}

		/**
		 * Enable SSH access while enabling other requisite services
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public static function unblockSsh(SiteConfiguration $svc): bool
		{
			$whiteout = new Whiteout($svc->getAccountRoot());
			return !$whiteout->blocked(self::PAM_WHITEOUT_FILE) || $whiteout->expose(self::PAM_WHITEOUT_FILE);
		}
	}