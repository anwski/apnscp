<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */
	if (defined('APNSCP_VERSION')) {
		return;
	}

	// @var array config.ini settings expanded to array
	$CONFIG_ARRAYS = [
		'HTTP_TRUSTED_FORWARD',
		'ANVIL_WHITELIST',
		'CGROUP_CONTROLLERS',
		'DNS_ALLOCATION_CIDR',
		'DNS_AUTHORITATIVE_NS',
		'DNS_HOSTING_NS',
		'DNS_VANITY_NS',
		'DNS_RECURSIVE_NS',
		'DNS_PROVIDERS',
		'LETSENCRYPT_ADDITIONAL_CERTS',
		'MAIL_PROVIDERS',
		'STYLE_BLACKLIST',
		'WEBAPPS_BLACKLIST',
		'LETSENCRYPT_DISABLED_CHALLENGES',
		'LETSENCRYPT_KEYID',
		'LETSENCRYPT_STAGING_KEYID',
		'TELEMETRY_COMPRESSION_THRESHOLD',
		'TELEMETRY_COMPRESSION_CHUNK'
	];

	$uname = posix_uname();
	$servershort = $server = $uname['nodename'];
	if (false !== ($pos = strpos($server, '.'))) {
		$servershort = substr($server, 0, $pos);
	}
	/** host to be used for setting cookies */

	$fstype = 'UNKNOWN';
	foreach (file('/proc/mounts', FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES) as $partition) {
		[$dev, $mount, $type, $extra] = explode(' ', $partition, 4);
		if ($mount === '/' && $dev[0] === '/') {
			// filter out "rootfs"
			$fstype = (0 === strncmp($type, 'ext', 3)) ? substr($type, 0, 3) : $type;
			break;
		}
	}
	if ($fstype !== 'xfs' && $fstype !== 'ext') {
		echo "unsupported filesystem type ${fstype} on /";
		exit(255);
	}

	$minuid = 500;
	if (file_exists('/etc/login.defs') && preg_match('/^[^#]*UID_MIN\s+(\d+)$/m', file_get_contents('/etc/login.defs'),
			$matches)) {
		$minuid = (int)$matches[1];
	}

	$selinux = (int)shell_exec('/sbin/selinuxenabled 2> /dev/null ; echo $?') === 0;
	// provided on at least coreutils 8.4.43
	$nproc = (int)shell_exec('/usr/bin/nproc');

	$constants = array(
		/**
		 * DO NOT ADJUST. WILL VOID WARRANTY AND SANITY
		 */
		'APNSCP_INSTALL_PATH'   => INCLUDE_PATH,
		'PRIVILEGE_USER'        => 0x1,
		'PRIVILEGE_SITE'        => 0x2,
		'PRIVILEGE_RESELLER'    => 0x4,
		'PRIVILEGE_ADMIN'       => 0x8,
		'PRIVILEGE_SERVER_EXEC' => 0x10,
		'PRIVILEGE_CHAIN'       => 0x20,
		'PRIVILEGE_NONE'        => 0x0,
		'PRIVILEGE_ALL'         => 1 | 2 | 4 | 8,

		'LINK_NORMAL'       => 0x0001,
		'LINK_TOP'          => 0x0002,
		'LINK_NEW'          => 0x0003,

		/** apnscp shared */
		'APNSCP_DOC_ROOT'   => '',
		'SERVER_NAME'       => $server,
		'SERVER_NAME_SHORT' => $servershort,

		'PHPPGADMIN_LOCATION' => 'https://' . $server . '/phpPgAdmin/',
		'PHPMYADMIN_LOCATION' => 'https://' . $server . '/phpMyAdmin/',
		/** graph constants */
		'PLATFORM_VERSION'    => platform_version(),
		'OS_VERSION'          => os_version(),
		// size of a filesystem block in KB
		'NPROC'               => $nproc,
		'APACHE_USER'         => 'apache',
		'BUG_REPORT'          => null,
		'VENDOR_PATH'         => INCLUDE_PATH . '/vendor',
		'FILESYSTEM_TYPE'     => $fstype,
		'SELINUX'             => $selinux,
		'USER_MIN_UID'        => $minuid
	);

	foreach (array('db.yaml', 'auth.yaml') as $yaml) {
		$file = conf_path($yaml);
		if (!file_exists($file)) {
			echo 'missing config/' . $yaml;
			exit(1);
		}
		$var = substr($yaml, 0, strpos($yaml, '.')) . 'yaml';
		$$var = \Symfony\Component\Yaml\Yaml::parseFile($file);
	}

	/**
	 * Config variation that flattens up to numerically indexed items
	 *
	 * @param array $array
	 * @param string $prepend
	 * @return array
	 */
	$flattenizer = static function(array $array, string $prepend = '') use (&$flattenizer)
	{
		$results = array();

		foreach ($array as $key => $value) {
			if (is_array($value)) {
				if (!is_int(key($value))) {
					$results = array_merge($results, $flattenizer($value, $prepend . $key . '.'));
				} else {
					$results[implode('.', [rtrim($prepend, '.'), $key]) ] = $value;
				}
			} else {
				$results[$prepend . $key] = $value;
			}
		}

		return $results;
	};
	$yamlconfig = array_build($flattenizer($authyaml, 'auth.') + $flattenizer($dbyaml, 'db.'), static function($k, $v) {
		return [strtoupper(str_replace('.', '_', $k)), $v];
	});

	// backwards compatible references
	$yamlconfig += array(
		// apnscp db
		'APNSCP_HOST'     => $yamlconfig['DB_APNSCP_HOST'],
		'APNSCP_DATABASE' => $yamlconfig['DB_APNSCP_DATABASE'],
		'APNSCP_USER'     => $yamlconfig['DB_APNSCP_USER'],
		'APNSCP_PASSWORD' => $yamlconfig['DB_APNSCP_PASSWORD'],
		// appliance db
		'POSTGRESQL_HOST'     => $yamlconfig['DB_APPLDB_HOST'],
		'POSTGRESQL_DATABASE' => $yamlconfig['DB_APPLDB_DATABASE'],
		'POSTGRESQL_USER'     => $yamlconfig['DB_APPLDB_USER'],
		'POSTGRESQL_PASSWORD' => $yamlconfig['DB_APPLDB_PASSWORD'],
		// soap db
		'SOAP_KEY_HOST'       => $yamlconfig['DB_API_HOST'],
		'SOAP_KEY_USER'       => $yamlconfig['DB_API_USER'],
		'SOAP_KEY_PASSWORD'   => $yamlconfig['DB_API_PASSWORD'],
		'SOAP_KEY_DB'         => $yamlconfig['DB_API_DATABASE'],
		// crm
		'CRM_TICKET_HOST'     => $yamlconfig['DB_CRM_HOST'],
		'CRM_TICKET_USER'     => $yamlconfig['DB_CRM_USER'],
		'CRM_TICKET_DB'       => $yamlconfig['DB_CRM_DATABASE'],
		'CRM_TICKET_PASSWORD' => $yamlconfig['DB_CRM_PASSWORD'],

		// billing
		'BILLING_HOST'        => $yamlconfig['DB_BILLING_HOST'],
		'BILLING_USER'        => $yamlconfig['DB_BILLING_USER'],
		'BILLING_PASSWORD'    => $yamlconfig['DB_BILLING_PASSWORD'],
		'BILLING_DB'          => $yamlconfig['DB_BILLING_DATABASE'],

		// Auth module
		// verify username global uniqueness
		'AUTH_USERNAME_HOST'         => $yamlconfig['DB_AUTH_HOST'],
		'AUTH_USERNAME_USER'         => $yamlconfig['DB_AUTH_USER'],
		'AUTH_USERNAME_PASSWORD'     => $yamlconfig['DB_AUTH_PASSWORD'],
		'AUTH_USERNAME_DB'           => $yamlconfig['DB_AUTH_DATABASE'],

		// DNS module
		'DNS_TSIG_KEY'               => $yamlconfig['AUTH_DNS_TSIG'] ?? null,

		// auth configuration
		'SMTP_USERNAME' => $yamlconfig['AUTH_SMTP_USERNAME'] ?? null,
		'SMTP_PASSWORD' => $yamlconfig['AUTH_SMTP_PASSWORD'] ?? null,
		'SMTP_HOST'     => $yamlconfig['AUTH_SMTP_HOST'] ?? null,
		'SMTP_PORT'     => (int)($yamlconfig['AUTH_SMTP_PORT'] ?? null),

		// CloudFlare
		'CLOUDFLARE_API_USER'        => $yamlconfig['AUTH_CLOUDFLARE_USER'] ?? null,
		'CLOUDFLARE_API_KEY'         => $yamlconfig['AUTH_CLOUDFLARE_KEY'] ?? null,

		// MaxMind GeoIP
		'MAXMIND_GEOIP_KEY'          => $yamlconfig['AUTH_MAXMIND_KEY'] ?? null,
		'MAXMIND_GEOIP_ID'           => $yamlconfig['AUTH_MAXMIND_ID'] ?? null
	);
	$constants = array_merge($constants, $yamlconfig);

	if (file_exists($file = conf_path('custom/constants.php'))) {
		$constants = array_replace($constants, include $file);
	}

	$files = glob(conf_path('custom/*.ini'));
	array_unshift($files, conf_path('config.ini'));
	foreach ($files as $f) {
		if (false === ($ini = parse_ini_file($f, true, INI_SCANNER_TYPED))) {
			fatal("failed parsing `%s'", $f);
		}
		foreach (array_keys($ini) as $section) {
			foreach ($ini[$section] as $k => $v) {
				$k = $section === 'core' ? strtoupper($k) : strtoupper($section . '_' . $k);
				$constants[$k] = $v;
			}
		}
	}

	foreach ($CONFIG_ARRAYS as $key) {
		if (!isset($constants[$key])) {
			continue;
		}
		// @todo convert to proper types?
		$constants[$key] = preg_split('/\s*,\s*/', $constants[$key], -1, PREG_SPLIT_NO_EMPTY);
	}

	// this should work, but if not...
	$mntpnt = rtrim(shell_exec('/bin/findmnt -fno SOURCE --target ' . $constants['FILESYSTEM_VIRTBASE'] . ' 2> /dev/null'));
	if (!$mntpnt) {
		echo 'Unable to discover apnscp mount point ' . $constants['FILESYSTEM_VIRTBASE'];
		exit(255);
	}

	$blocksz = (int)shell_exec('/sbin/blockdev --getbsz ' . escapeshellarg($mntpnt) . ' 2> /dev/null') / 1024;
	// assume 4KB block size
	if (!$blocksz) {
		$blocksz = 4;
	}

	$constants += [
		'FILESYSTEM_BLKSZ'    => $blocksz,
		'FILESYSTEM_MOUNTPOINT' => $mntpnt
	];

	/** apache uid used in file_* functions */
	$pwd = posix_getpwnam($constants['APACHE_USER']);
	$apacheuid = $pwd['uid'];
	$apachegid = $pwd['gid'];

	$constants['WS_USER'] = $constants['APNSCP_SYSTEM_USER'];
	$pwd = posix_getpwnam($constants['WS_USER']);
	$constants['WS_UID'] = $pwd['uid'];
	$constants['WS_GID'] = $pwd['gid'];
	$constants['APACHE_UID'] = $apacheuid;
	$constants['APACHE_GID'] = $apachegid;
	if ($constants['APNSCPD_HEADLESS']) {
		$constants['SOAP_ENABLED'] = false;
	}
	if (empty($constants['TIMEZONE'])) {
		$constants['TIMEZONE'] = date_default_timezone_get();
	}
	if (empty($constants['LOCALE'])) {
		$constants['LOCALE'] = setlocale(LC_ALL, '');
	}

	if (empty($constants['CACHE_SUPER_GLOBAL_HOST'])) {
		$constants['CACHE_SUPER_GLOBAL_HOST'] = run_path(\Cache_Mproxy::REDIS_SOCKET);
		$constants['CACHE_SUPER_GLOBAL_PORT'] = null;

	}
	// allow relative paths
	if ($constants['APNSCPD_SOCKET'][0] !== '/') {
		$constants['APNSCPD_SOCKET'] = INCLUDE_PATH . '/' . $constants['APNSCPD_SOCKET'];
	}

	$constants['PHPMYADMIN_LOCATION'] = str_replace('http://', 'https://', $constants['PHPMYADMIN_LOCATION']);
	$constants['PHPPGADMIN_LOCATION'] = str_replace('http://', 'https://', $constants['PHPPGADMIN_LOCATION']);

	// convert from bytes to KB
	$constants['MEM_PAGESIZE'] = (int)shell_exec('getconf PAGESIZE') / 1024;
	// jiffies
	$constants['CPU_CLK_TCK'] = (int)shell_exec('getconf CLK_TCK');
	$constants['CFG_DEBUG'] = $constants['DEBUG'];
	$constants['CFG_VERBOSE'] = $constants['DEBUG_BACKTRACE_QUALIFIER'];
	$constants['CACHE_SERIALIZER'] = constant("Redis::SERIALIZER_" . strtoupper($constants['CACHE_SERIALIZER'] ?: 'none'));
	$constants['CACHE_COMPRESSION'] = constant("Redis::COMPRESSION_" . strtoupper($constants['CACHE_COMPRESSION'] ?: 'none'));
	return $constants;