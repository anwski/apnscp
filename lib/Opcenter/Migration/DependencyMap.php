<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Migration;

use Opcenter\Service\ModulePriority;

class DependencyMap {

	/**
	 * @var array
	 */
	protected $modules;

	/**
	 * Order dependencies
	 *
	 * @param array $modules
	 */
	public function __construct(array $modules)
	{
		$this->modules = $modules;
	}

	/**
	 * Order path maps
	 *
	 * @return array
	 */
	public function order(): array
	{
		return ModulePriority::prioritize(array_keys($this->modules), $this->modules);
	}
}