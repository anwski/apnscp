<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Lararia\Jobs\Job;

	class WebappFailureDigestReport extends Mailable
	{
		use SerializesModels;
		protected $job;

		/**
		 * Create a new message instance.
		 *
		 * @param Job\Report $report
		 */
		public function __construct(Job\Report $report)
		{
			$this->job = $report->current();
		}

		public function build()
		{
			return $this->markdown('email.webapps.failure-digest',
				[
					'report' => (array)$this->job->getReturn()
				]
			)->subject('Web App Digest')
				->to($this->job->common_get_email());
		}
	}