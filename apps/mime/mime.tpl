<?php
?>
<!-- main object data goes here... -->
<form method="POST">
	<table width="100%">
		<tr>
			<td colspan="3" class="head4" align="center">
				Whois Record
			</td>
		</tr>
		<tr>
			<td class="cell1" width="90">
				Domain name:
			</td>
			<td colspan=2 class="cell1"><input type="text" name="Domain" value="<?php print(@$_POST['Domain']); ?>"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="cell1">
				<input type="submit" value="Lookup"/>
			</td>
		</tr>
		<?php
    if ($Page->is_postback && !$Page->errors_exist()): ?>
		<tr>
			<td class="cell1" align="left" colspan="3" style="padding-left:5px;">
				<code><pre><?php print($Page->getWhoisRecord($_POST['Domain']));?>
            </pre>
				</code>
			</td>
		</tr>
		<?php
    endif;
    ?>
	</table>
</form>