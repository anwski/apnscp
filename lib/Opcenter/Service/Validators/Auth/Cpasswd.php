<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Auth;

	use Opcenter\Auth\Shadow;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Cpasswd extends Common implements ServiceReconfiguration, ServiceInstall
	{
		const DESCRIPTION = 'Crypted password (salt required)';

		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			if (!$value && !$this->isSet('tpasswd', 'passwd')) {
				return $this->ctx->hasOld() ? true :
					error('no password provided on account');
			}
			// preserve tpasswd for notify support
			$this->clear('passwd');
			if (!Shadow::valid_crypted($value)) {
				return false;
			}

			return true;
		}

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure('', '', $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$passwd = $this->ctx->getServiceValue(null, 'cpasswd');
			if (!$passwd) {
				return true;
			}
			$this->site = $svc->getSite();
			$admin = $this->ctx->getServiceValue('siteinfo', 'admin_user');
			if (!Shadow::bindTo($this->domain_fs_path())->set_cpasswd($passwd,
				$admin)) {
				return false;
			}
			if (!$this->ctx->getServiceValue('auth', \Auth_Module::PWOVERRIDE_KEY) &&
				!$this->ctx->getOldServiceValue('auth', \Auth_Module::PWOVERRIDE_KEY))  {
				$this->freshenSite($svc);
			}
			$this->ctx->set('cpasswd', null);

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function write()
		{
			return null;
		}

	}