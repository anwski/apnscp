<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Client implementation of various SASL mechanisms
	 *
	 * @author  Richard Heyes <richard@php.net>
	 * @access  public
	 * @version 1.0
	 * @package Auth_SASL
	 */
	class Auth_SASL
	{
		/**
		 * Factory class. Returns an object of the request
		 * type.
		 *
		 * @param string $type         One of: Anonymous
		 *                             Plain
		 *                             CramMD5
		 *                             DigestMD5
		 *                             Types are not case sensitive
		 */
		function &factory($type)
		{
			switch (strtolower($type)) {
				case 'anonymous':
					$filename = 'Auth/SASL/Anonymous.php';
					$classname = 'Auth_SASL_Anonymous';
					break;

				case 'login':
					$filename = 'Auth/SASL/Login.php';
					$classname = 'Auth_SASL_Login';
					break;

				case 'plain':
					$filename = 'Auth/SASL/Plain.php';
					$classname = 'Auth_SASL_Plain';
					break;

				case 'external':
					$filename = 'Auth/SASL/External.php';
					$classname = 'Auth_SASL_External';
					break;

				case 'crammd5':
					$filename = 'Auth/SASL/CramMD5.php';
					$classname = 'Auth_SASL_CramMD5';
					break;

				case 'digestmd5':
					$filename = 'Auth/SASL/DigestMD5.php';
					$classname = 'Auth_SASL_DigestMD5';
					break;

				default:
					return PEAR::raiseError('Invalid SASL mechanism type');
					break;
			}

			require_once($filename);
			$obj = new $classname();

			return $obj;
		}
	}

?>
