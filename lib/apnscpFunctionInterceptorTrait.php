<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Trait apnscpFunctionInterceptorTrait
	 * @mixin \Module_Skeleton
	 */
	trait apnscpFunctionInterceptorTrait
	{
		/**
		 * @var apnscpFunctionInterceptor
		 */
		protected $apnscpFunctionInterceptor;

		/**
		 * Call another module method
		 *
		 * @param string $function
		 * @param mixed  $args
		 * @return bool
		 */
		public function __call($function, $args = null)
		{
			return ($this->apnscpFunctionInterceptor ?? \apnscpFunctionInterceptor::init())->call($function, $args);
		}

		/**
		 * @return \Module_Skeleton
		 */
		protected function getApnscpFunctionInterceptor(): \apnscpFunctionInterceptor
		{
			return $this->apnscpFunctionInterceptor ?? \apnscpFunctionInterceptor::init();
		}

		protected function setApnscpFunctionInterceptor(\apnscpFunctionInterceptor $afi): void
		{
			$this->apnscpFunctionInterceptor = $afi;
		}
	}