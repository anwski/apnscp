<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Module\Definition;

	interface MODULE30
	{
		// initial release of apnscp codename "Paixhans"
		public function setUserParameters(\Auth_Info_User $auth);

		public static function autoloadModule(\Auth_Info_User $context): \Module_Skeleton;

		public function getExportedFunctions(): array;

		public function cleanUserParameters();

		public function query($cmd, ...$args);

		public function __wakeup();

		public function _invoke($function, $args);
	}