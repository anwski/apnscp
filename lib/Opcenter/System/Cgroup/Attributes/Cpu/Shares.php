<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */


	namespace Opcenter\System\Cgroup\Attributes\Cpu;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;

	class Shares extends BaseAttribute
	{
		const WEIGHT_DEFAULT = 100;
		const CPU_SHARES_DEFAULT = 1024;

		/**
		 * Convert from shares to weight
		 *
		 * @return int
		 */
		public function toWeight(): int {
			return (int)($this->value * static::WEIGHT_DEFAULT / static::CPU_SHARES_DEFAULT);
		}

		/**
		 * Convert from weight to shares
		 *
		 * @return int
		 */
		public function fromWeight(): int {
			return (int)($this->value * static::CPU_SHARES_DEFAULT / static::WEIGHT_DEFAULT);
		}

		public function getValue()
		{
			return $this->fromWeight();
		}


	}