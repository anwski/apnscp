---
# Default backend invoked with:
# ntfy send "msg"
backends:
  - default
# Backend configuration
# See also https://github.com/dschep/ntfy
default: &default
  backend: pushover
  # May be overridden with -t title
  title: "{{ SERVER_NAME_SHORT }}"
  api_token: TOKEN
  user_key: KEY
# High priority backend ntfy -b high send "msg"
high:
  title: "❗ {{ SERVER_NAME_SHORT }}"
  priority: 2
  expire: 3600
  retry: 120
  <<: *default
