<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\soapkeys;

	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('keys.js');
			$this->init_js('prism');
			$this->add_css('button.ui-action { background-color: transparent; } ', 'internal');
			$this->add_css('keys.css');
		}

		public function on_postback($params)
		{
			if (isset($params['Generate_Key'])) {
				$comment = null;
				if (isset($params['comment'])) {
					$comment = trim($params['comment']);
				}

				return $this->auth_create_api_key($comment);
			} else if (isset($params['edit'])) {
				$key = $params['edit'];
				$comment = null;
				if (isset($params['comment'])) {
					$comment = $params['comment'];
				}

				return $this->auth_set_api_key_comment($key, $comment);
			} else if (isset($params['delete'])) {
				$key = $params['delete'];

				return $this->auth_delete_api_key($key);
			}
		}

		public function _render()
		{
			$this->view()->share([
				'soapkeys' => $this->getSoapKeys(),
				'langs'    => [
					'PHP' => [
						'icon' => 'php-w'
					]
				]
			]);
		}

		public function getSoapKeys()
		{
			return $this->auth_get_api_keys();
		}

		public function soapUri(): string {
			return 'https://' . $this->formatUri() . ':' . \Util_API::SECURE_PORT;
		}

		public function formatUri(): string
		{
			$server = \Auth_Redirect::CP_LAYOUT ? SERVER_NAME_SHORT : SERVER_NAME;

			return \Auth_Redirect::makeCPFromServer($server);
		}

	}