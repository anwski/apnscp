<div id="authentication" class="tab-pane" role="tabpanel" aria-labelledby="">
	@if (\UCard::is('site'))
		@include('partials.third-party.google-analytics')
		@include('partials.third-party.maxmind')
		@includeWhen($Page->hasMagento(), 'partials.third-party.magento')
	@endif
</div>