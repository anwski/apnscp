<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */


	namespace Frontend\Css;

	use Opcenter\Blacklist;

	class StyleManager
	{
		const THEME_PATH = 'css/themes/custom';
		const THEME_CUSTOM_KEY = 'theme.custom';

		// @var string style filename
		protected $filename;
		// @var string hmac hash
		protected $hash;

		public function __construct(string $fileName)
		{
			if ($fileName[0] != '/') {
				$fileName = public_path(self::THEME_PATH . '/' . $fileName);
			}
			// this should come in through a secured channel, but just in case
			$fileName = str_replace('..', '', $fileName);
			$this->filename = $fileName;
		}

		public static function isActive()
		{
			return \Preferences::get(\Page_Renderer::THEME_KEY) ===
				'custom/' . \Preferences::get(self::THEME_CUSTOM_KEY);
		}

		/**
		 * Given theme name is valid
		 *
		 * @return bool
		 */
		public function valid()
		{
			if (!file_exists($this->filename)) {
				return false;
			}
			if (!\Util_HTTP::upload_safe($this->filename) &&
				0 !== strpos(public_path() . self::THEME_PATH, $this->filename)) {
				return false;
			}
			if (!is_file($this->filename)) {
				return false;
			}

			return !is_link($this->filename);
		}

		public function save()
		{
			if (self::exists()) {
				// remove old theme first
				$path = public_path(self::THEME_PATH . '/' . self::getTheme() . '.css');
				if (file_exists($path)) {
					unlink($path);
				}
			}
			$path = public_path(self::THEME_PATH . '/' . $this->hash()) . '.css';
			$contents = file_get_contents($this->filename);
			if (!file_put_contents($path, $contents)) {
				return error('failed to save custom theme');
			}
			\Preferences::set(self::THEME_CUSTOM_KEY, $this->hash());
			\Preferences::set(\Page_Renderer::THEME_KEY, 'custom/' . $this->hash());

			return $this->remove();
		}

		/**
		 * @return bool
		 */
		public static function exists()
		{
			$pref = \Preferences::get(self::THEME_CUSTOM_KEY);
			if (!$pref) {
				return false;
			}

			return true;
		}

		public static function getTheme()
		{
			return \Preferences::get(self::THEME_CUSTOM_KEY);
		}

		/**
		 * Get hash of file
		 *
		 * @return string
		 */
		protected function hash()
		{
			if (null === $this->hash) {
				$this->hash = hash_hmac_file('sha384', $this->filename, md5(random_bytes(60)));
			}

			return $this->hash;
		}

		public function remove()
		{
			return unlink($this->filename);
		}

		/**
		 * User can set custom themes
		 *
		 * @return bool
		 */
		public static function allowCustom(): bool
		{
			return (bool)STYLE_ALLOW_CUSTOM;
		}

		/**
		 * User can change style
		 *
		 * @return bool
		 */
		public static function allowSelection(): bool
		{
			return !STYLE_BLACKLIST || \count(STYLE_BLACKLIST) > 1 || STYLE_BLACKLIST[0] !== '*';
		}

		/**
		 * Get system themes for apnscp
		 *
		 * @return array
		 */
		public static function getThemes(): array
		{
			static $themes;

			if ($themes === null) {
				$themes = [];
				if (!self::allowSelection()) {
					return $themes;
				}

				$files = array_filter(array_map(
					static function ($file) {
						$theme = basename($file, '.css');
						if (false !== strpos($theme, '.')) {
							return null;
						}
						return $theme;
					},
					glob(public_path('/css/themes/[^\$]*.css'))
				));
				$themes = (new Blacklist(STYLE_BLACKLIST))->filter($files);
				if (STYLE_THEME && !\in_array(STYLE_THEME, $themes, true)) {
					$themes[] = STYLE_THEME;
				}
				asort($themes);
			}
			return $themes;
		}
	}