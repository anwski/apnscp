<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Providers\Mailbot\Options;

	use Opcenter\Mail\Vacation\GenericOption;

	class Message extends GenericOption
	{
		use \FilesystemPathTrait;
		use \apnscpFunctionInterceptorTrait;

		const VACATION_FILE = '.vacation.msg';
		const FLAG = '';
		const DEFAULT = 'I am currently away from the office.  Your message will be read ' .
		'once I return.' . "\n\n" . 'Thank you.';

		public function __construct()
		{
			// @var string required by FilesystemPathTrait
			$this->site = $this->getAuthContext()->site;
		}

		public function getValue(): ?string
		{
			$username = $this->getAuthContext()->username;

			return $this->getFromUser($username);
		}

		/**
		 * Get a vacation message from a user
		 *
		 * @param string $username
		 * @return string
		 */
		public function getFromUser(string $username): string
		{
			$home = $this->user_get_home($username);
			$file = $home . '/' . self::VACATION_FILE;
			if (!$this->file_exists($file)) {
				return static::DEFAULT;
			}

			return (string)$this->file_get_file_contents($file);
		}

		public function setValue($value): bool
		{
			if (!trim($value)) {
				return error('missing vacation message');
			}
			$username = $this->getAuthContext()->username;
			$ret = $this->setValueByUser($username, $value);
			if ($ret) {
				$this->value = $value;
			}

			return $ret;
		}

		public function setValueByUser(string $user, $value): bool
		{
			$pwd = $this->user_getpwnam($user);
			if (!$pwd) {
				return false;
			}
			$file = $pwd['home'] . '/' . self::VACATION_FILE;

			return $this->file_put_file_contents($file, $value) &&
				$this->file_chmod($file, 600) &&
				$this->file_chown($file, (int)$pwd['uid']
				);
		}
	}
