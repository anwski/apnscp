<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}

			if ($this->ctx->isRevalidation()) {
				// provider changed
				// @TODO ambiguous properties, conflicts with forceRevalidation
				$this->force = true;

			}

			return true;
		}


		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$ctx = $svc->getAuthContext();
			$class = \apnscpFunctionInterceptor::get_autoload_class_from_module('dns');
			$module = $class::instantiateContexted($ctx)->_proxy();
			if (!$module->configured()) {
				return info("DNS not configured for `%s', bypassing DNS hooks", $ctx->domain);
			}

			$ip = (array)$module->get_public_ip();
			foreach ($this->getDnsDomains($svc) as $domain) {
				if (!$module->add_zone($domain, $ip[0])) {
					return false;
				}

				if (!$module->domain_uses_nameservers($domain)) {
					warn("Domain `%s' doesn't use assigned nameservers. Change nameservers to %s",
						$domain, implode(',', $module->get_hosting_nameservers($domain))
					);
				}
			}


			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			// @var Dns_Module $dns
			if ($this->ctx->isEdit() && $this->ctx['provider'] !== 'null')
			{
				return warn("DNS is preserved when setting dns,enabled=0 on edit. " .
					"To remove DNS for zone set both dns,enabled=0 and dns,provider=null.");
			}
			$class = \apnscpFunctionInterceptor::get_autoload_class_from_module('dns');
			$module = $class::instantiateContexted($svc->getAuthContext())->_proxy();
			$rfxn = new \ReflectionProperty($module, 'permission_level');
			$rfxn->setAccessible(true);
			$rfxn->setValue($module, $rfxn->getValue($module) | PRIVILEGE_SERVER_EXEC);
			foreach ($this->getDnsDomains($svc) as $domain) {
				debug("Try-remove zone `%s'", $domain);
				try {
					if (!$module->remove_zone($domain)) {
						dlog("Could not remove zone `%s'", $domain);
					}
				} catch (\Throwable $e) {
					if (!$svc->hasValidatorOption('force')) {
						throw $e;
					}
					warn("Failed to remove zone, force applied. Ignoring: %s", $e->getMessage());
				}
			}

			return true;
		}

		/**
		 * Get domains for which DNS is provided
		 *
		 * A domain may be attached to an account without a document root assigned.
		 * Use a conservative approach to get check aliases,aliases + siteinfo,domain
		 *
		 * @param SiteConfiguration $svc
		 * @return array
		 */
		private function getDnsDomains(SiteConfiguration $svc): array {
			return $svc->getSiteFunctionInterceptor()->aliases_list_aliases() + append_config([
					$svc->getServiceValue('siteinfo', 'domain')
				]);
		}
	}

