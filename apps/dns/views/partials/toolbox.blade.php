<button class="ui-action-label ui-action ui-action-toolbox toolbox btn btn-secondary dropdown-toggle"
        aria-haspopup="true" aria-expanded="false" type="button" data-toggle="dropdown">
	Toolbox
</button>
<div class="dropdown-menu dropdown-menu-right" id="toolbox">
	<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
	   data-target="#domainDetails" aria-controls="#domainDetails" aria-expanded="false"
		title="Show advanced information about domain" id="toolbox-dns-details">
		Show DNS details
	</a>
	<hr class="my-0"/>
	@if (!$Page->bypassed())
		<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
		   title="Delegate email to your hosting mail servers" id="toolbox-mx-apnscp">Restore default
			MX</a>
	@endif
	<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
	title="Delegate email for this domain to Gmail" id="toolbox-mx-gmail">
		Set MX to Gmail
	</a>
	@if ($Page->domain_ns)
		<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
		   title="Copy MX records from the assigned nameserver {{ $Page->domain_ns }}
				   to {{ $Page->primary_ns }}"
		   id="toolbox-mx-clone">
			External NS MX records
		</a>
	@endif
	<hr class="my-0" />
	<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
	   title="Download a copy of all DNS information for this zone" id="toolbox-export">
		Export zone
	</a>
	@if (\count($Page->getAvailableDomains()) > 1)
		<a href="#" class="dropdown-item" data-toggle="tooltip" data-placement="left"
		title="Clone DNS from existing domain" id="toolbox-clone-domain">
			Clone DNS zone
		</a>
	@endif
	@if (!$Page->bypassed())
		<a href="#" class="warn btn btn-block rounded-0 dropdown-item" data-toggle="tooltip" data-placement="left"
		   title="Remove ALL custom DNS modifictions and reset DNS to default settings"
		   id="toolbox-dns-restore">
			Reset DNS to default
		</a>
	@endif
	@if ($Page->bypassed())
		<div class="dropdown-divider"></div>
		<div class="dropdown-item">
			<span class="dropdown-item-text">
				<b>Foreign</b>: {{ \Auth_Redirect::lookup($Page->getDomain()) ?? 'UNKNOWN' }}
			</span>
		</div>
	@endif
</div>